Basic command line tool that compresses text using a sort of run length
encoding but a little better, 1 is not included (2 probably shouldn't either I
guess).

Compressed string is guaranteed to be at least the same size as the
original string. Does not work with numbers. Probably never will since this was
just made as an intro to rust for me.

Usage:

```bash
$ simple-compress --help
Simple compression tool using run length encoding. 0.1
Darrien G <me@darrien.dev>

USAGE:
    simple-compress [FLAGS] <INPUT>

FLAGS:
    -d, --decompress    Decompress data
    -h, --help          Prints help information
    -V, --version       Prints version information

ARGS:
    <INPUT>    Text to compress
$ simple-compress aaaaaaaaaaaaa
13a
$ simple-compress aaaaaaaaaaaaa | xargs simple-compress -d
aaaaaaaaaaaaa
```
