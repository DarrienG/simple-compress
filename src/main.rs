extern crate clap;

fn get_magnitude(num: &u32) -> u32 {
    let mut magnitude = 10;
    let mut repr = num.clone();

    while repr > 9 {
        repr /= 10;
        magnitude *= 10;
    }

    return magnitude;
}

fn get_compress_text(current_char: char, count: u32) -> String {
    if count <= 1 {
        return current_char.to_string();
    }

    return format!("{}{}", count, current_char);
}

fn decompress(text: &str) -> String {
    let mut num_chars = 0;
    let mut decompress_text = "".to_owned();
    for c in text.chars() {
        if c.is_numeric() {
            if num_chars > 0 {
                num_chars = num_chars * get_magnitude(&num_chars) + c.to_digit(10).unwrap();
            } else {
                num_chars = c.to_digit(10).unwrap();
            }
        } else {
            if num_chars == 0 {
                num_chars += 1;
            }
            for _ in 0..num_chars {
                decompress_text.push(c);
            }
        }
    }
    return decompress_text;
}

fn compress(text: &str) -> String {
    let mut compress_text = "".to_owned();
    let mut current_char = ' ';
    let mut count = 0;
    let mut first_run = true;

    for c in text.chars() {
        if !first_run {
            count += 1;
        }
        if c.is_numeric() {
            panic!("Illegal character: {} - number glyphs not supported.", c);
        }

        if current_char != c && !first_run {
            compress_text.push_str(&get_compress_text(current_char, count));
            count = 0;
        }

        current_char = c;
        first_run = false;
    }

    compress_text.push_str(&get_compress_text(current_char, count + 1));

    return compress_text;
}

fn main() {
    let args = clap::App::new("Simple compression tool using run length encoding.")
        .version("0.1")
        .author("Darrien G <me@darrien.dev>")
        .arg(
            clap::Arg::with_name("INPUT")
                .help("Text to compress")
                .required(true)
                .index(1),
        )
        .arg(
            clap::Arg::with_name("decompress")
                .short("d")
                .long("decompress")
                .value_name("decompress")
                .takes_value(false)
                .help("Decompress data"),
        )
        .get_matches();

    let input = args.value_of("INPUT").unwrap();
    if args.is_present("decompress") {
        println!("{}", decompress(input));
    } else {
        println!("{}", compress(input));
    }
}
